#!/usr/bin/perl

use strict;
use utf8;

use POSIX qw(locale_h);

use constant GCSTAR_LIB_ROOT => 'gcstar/lib/gcstar/';

use lib GCSTAR_LIB_ROOT;
use GCLang;

$ENV{GCS_LIB_DIR} = GCSTAR_LIB_ROOT;

use GCOptions;
my $options = new GCOptionLoader($ENV{GCS_CONFIG_FILE}, 1);

my $lang = $options->getFullLang;
$ENV{LANG} = $lang;
$ENV{LANGUAGE} = $lang;
$ENV{LC_ALL} = $lang;
$ENV{LC_CTYPE} = $lang;
setlocale(LC_ALL, $lang);

if ($ARGV[0] eq '--errors-only')
{
    $ENV{GCS_ERRORS_ONLY} = 'YES';
    shift @ARGV;
}

GCLang::checkLangs(@ARGV);
